/**
 * @typedef {Object} User
 * @property {number} id
 * @property {string} username
 * @property {string} full_name
 * @property {string} email
 * @property {string} role
 * @property {string} profile_pic
 */


