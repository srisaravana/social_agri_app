import FarmerApp from '@src/views/farmers/FarmerApp';
import FarmerHomePage from '@src/views/farmers/FarmerHomePage';


export const farmersRoutes = [
  {
    path: '/farmer',
    component: FarmerApp,
    children: [
      {
        path: '',
        name: 'farmerHomePage',
        component: FarmerHomePage,
      },
    ],

    meta: {
      requiresAuth: true,
      hasAccess: ['FARMER'],
    },
  },

];
