import {userRoutes} from '@src/router/groups/users';
import AdminApp from '@src/views/admin/AdminApp';
import AdminHomePage from '@src/views/admin/AdminHomePage';

export const adminRoutes = [
  {
    path: '/admin',

    component: AdminApp,
    children: [
      {
        path: '',
        name: 'adminHome',
        component: AdminHomePage,
      },
      ...userRoutes,
    ],
    meta: {
      requiresAuth: true,
      hasAccess: ['ADMIN'],
    },
  },
];
