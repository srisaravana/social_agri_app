import {adminRoutes} from '@src/router/groups/admin';
import {farmersRoutes} from '@src/router/groups/farmers';
import {pagesRoutes} from '@src/router/groups/pages';
import {store} from '@src/store';
import Login from '@src/views/Login';
import Page404 from '@src/views/pages/Page404';
import {createRouter, createWebHashHistory} from 'vue-router';

const routes = [

  {
    path: '/login',
    name: 'Login',
    component: Login,
  },

  ...pagesRoutes,
  ...adminRoutes,
  ...farmersRoutes,
  {
    path: '/:pathMatch(.*)*',
    name: '404',
    component: Page404,
  },
];


export const router = createRouter( {
  history: createWebHashHistory(),
  routes: routes,
} );


/**
 * To make sure only authenticated pages can be viewed if logged in
 * otherwise redirect to login page
 */
router.beforeEach( ( to, from, next ) => {
  const userType = store.getters[ 'auth/getUserType' ];
  const isLoggedIn = store.getters[ 'auth/getLoginStatus' ];

  if ( to.matched.some( record => record.meta.requiresAuth ) ) {

    if ( to.meta.hasOwnProperty( 'hasAccess' ) ) {
      if ( !to.meta.hasAccess.includes( userType ) ) {
        next( '/login' );
      }
    }

    if ( isLoggedIn ) {
      next();
    } else {
      next( '/login' );
    }

  } else {
    next();
  }
} );


// export default router;
