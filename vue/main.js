/* axios*/
import axios from 'axios';
import 'bootstrap';
import 'bootstrap-icons/font/bootstrap-icons.css';
import 'datatables.net-bs5/css/dataTables.bootstrap5.min.css';


import moment from 'moment';
import {createApp} from 'vue';

import App from './App';
import {errorDialog} from './assets/libs/bootloks';
import './assets/libs/daterangepicker/daterangepicker';

/* daterangepicker */
import './assets/libs/daterangepicker/daterangepicker.css';

/* local common scss */
import './assets/scss/common.scss';
import {router} from './router/index';
import {store} from './store/index';

window.moment = moment;
window.bootstrap = require( 'bootstrap/dist/js/bootstrap.bundle.min' );


/* datatables */
require( 'datatables.net-bs5' );
/* css file for datatable is loaded with main scss file under assets/scss/datatables.css */

/* Axios configurations */
const currentDomain = window.location.origin;
axios.defaults.baseURL = `${ currentDomain }/api`;
axios.defaults.headers[ 'auth' ] = store.getters[ 'auth/getAuthKey' ];

/*
* Axios intercepting incoming responses, to check if it is 401,
* then route to login page, because auth token is expired, or
* not logged in
* */
axios.interceptors.response.use( undefined, ( error => {
  if ( error.response.status === 401 ) {
    store.dispatch( 'auth/logout' ).then( () => {
      router.push( '/login' ).then();
      errorDialog( { message: 'Authentication failed. Please login with proper credentials.', id: 'auth-failed' } );
    } );
  }
  return Promise.reject( error );
} ) );


/* set document title */
document.title = 'Farmer Buddy';

/* Vue initialization */
createApp( App ).use( router ).use( store ).mount( '#app' );
